import requests
import base64
from enumMode import Mode

# Get OAuth Token
# Docs: https://developer.ebay.com/api-docs/static/oauth-scopes.html#assignment
# Docs: https://developer.ebay.com/api-docs/static/oauth-client-credentials-grant.html

def getAccessToken(client_id, client_secret, mode):
    token = requests.post(
        mode.value + '/identity/v1/oauth2/token', 
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + base64.b64encode((client_id + ':' + client_secret).encode('utf-8')).decode('utf-8')
        }, 
        data = {
        'grant_type': 'client_credentials',
        'scope': 'https://api.ebay.com/oauth/api_scope'
        }
    ).json().get('access_token')

    return "Bearer " + token
