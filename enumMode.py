from enum import Enum

class Mode(Enum):
    PRODUCTION = 'https://api.ebay.com'
    SANDBOX = 'https://api.sandbox.ebay.com'
