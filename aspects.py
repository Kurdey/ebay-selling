import requests
import json
from enumMode import Mode

# Get the attributes (aka aspects) of the books I want to sell
# Docs: https://developer.ebay.com/api-docs/commerce/taxonomy/resources/category_tree/methods/getDefaultCategoryTreeId

def getGermanBookApects(token, mode):

    marketplace_id = 'EBAY_DE'

    localCategoryTree = requests.get(
        mode.value + '/commerce/taxonomy/v1/get_default_category_tree_id', 
        headers = {
            'Authorization': token
        },
        params={
            'marketplace_id': marketplace_id
        }
    ).json()

    categoryTreeId = localCategoryTree.get('categoryTreeId')
    categoryTreeVersion = localCategoryTree.get('categoryTreeVersion')

    categoryTree = json.loads(requests.get(
        mode.value + '/commerce/taxonomy/v1/category_tree/77',
        headers = {
            'Authorization': token
        }
    ).text)

    # Go through all the categories ebay offers and extract the ID of the one called "Bücher"

    bookCategory = 0
    for x in categoryTree['rootCategoryNode']['childCategoryTreeNodes']:
        for y in x['childCategoryTreeNodes']:
            if y['category']['categoryName'] == "Bücher":
                bookCategory = y['category']['categoryId']

    # Get the attributes (aspects) associated with the "Bücher" category

    aspects = json.loads(requests.get(
        mode.value + '/commerce/taxonomy/v1/category_tree/' + categoryTreeId + '/get_item_aspects_for_category',
        headers = {
            'Authorization': token
        },
        params={
            'category_id': bookCategory
        }
    ).text)['aspects']

    aspectsArray = []

    for a in aspects:
        aspectsArray.append(a['localizedAspectName'])

    print(aspectsArray)


