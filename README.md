# Ebay Selling

## Goal of this repo

As I was gifted a book collection I was looking for an efficient way to offer the individual books on ebay.
Because I did not find helpful resources on this, I am providing my code here, for it may help you with your selling project.

Because my flat is full of books, this project is also focused on books. You might want to adapt the code to your needs.

## How to get this project running

1. You need Python installed on your machine
2. Get the credentials
3. Rename the credentials.sample.json into credentials.json and fill in the credentials provided from ebay 

## How the ebay API works


