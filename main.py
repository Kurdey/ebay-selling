import json
import requests
# import all the functions from the other files
from accessToken import *
from aspects import *
from enumMode import *

# Load credentials from file
c = json.load(open('credentials.json'))

# Get an Bearer token in order to authenticate further requests
token = getAccessToken(c['client_id'], c['client_secret'], Mode.SANDBOX)

# Only required once to find out the attributes that your listings have
getGermanBookApects(token, Mode.SANDBOX)